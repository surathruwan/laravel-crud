@extends('master')

@section('content')

<div class="row">
    <div class="col-md-8 offset-2">
        <br>
        <h3 aling="center">Add Data</h3>
        <br>
        
        @if(count($errors) > 0)

        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(\Session::has('success'))
        <div class="alert alert-success">
           <p style="margin-bottom:0">{{\Session::get('success') }}</p>
        </div>  
        @endif

        <form method="post" action="{{ route('student.store') }}">
            {{csrf_field()}}
            <div class="form-group">
                <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" />
            </div>

            <div class="form-group">
                <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" />
            </div>

            <div class="form-group">
                <input type="submit"  class="btn btn-primary" placeholder="Enter Last Name" value="Submit" />
            </div>
        
        </form>

    </div>
</div>

@endsection