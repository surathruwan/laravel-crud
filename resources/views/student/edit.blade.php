@extends('master')

@section('content')

<div class="row">
    <div class="col-md-8 offset-2">
    <br/>
    <h3 align="center">Edit Record</h3>
    <br/>
    @if(count($errors) > 0)

    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
  
    @endif
    <form method="post" action="{{action('StudentController@update',$id) }}">
        {{csrf_field()}}

        <input type="hidden" name="_method" value="PATCH" />
        <div class="form-group">
                <input type="text" name="first_name" value="{{$student->first_name}}" class="form-control" placeholder="Enter First Name" />
            </div>

            <div class="form-group">
                <input type="text" name="last_name" value="{{$student->last_name}}"  class="form-control" placeholder="Enter Last Name" />
            </div>

            <div class="form-group">
                <input type="submit"  class="btn btn-primary" placeholder="Enter Last Name" value="Edit" />
            </div>
    </form>
    </div>
    </div>
</div>

@endsection